import json
from datetime import datetime, timedelta
from decimal import Decimal

from django.db.models import Sum, Count
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template import loader
from django.views.generic import ListView, CreateView, UpdateView, TemplateView

from .forms import TransactionForm
from .models import Transaction, Category, Plan


def index(request):
    qs = Transaction.objects.get_queryset()
    metrics = {
        'transaction_count': Count('id'),
        'quantity__sum': Sum('quantity'),
    }
    transactions_summary = list(
        qs.values('category__name').annotate(**metrics).order_by('-quantity__sum')
    )
    transactions_total = dict(
        qs.aggregate(**metrics)
    )
    context = {
        'summary': json.dumps(transactions_summary, cls=DecimalEncoder),
        'total': json.dumps(transactions_total, cls=DecimalEncoder),
    }
    return render(request, "index.html", context)


#####################
# extent json encoder
class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


def transaction_new(request):
    if request.method == "POST":
        form = TransactionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = TransactionForm()
    context = {'form': form}
    template = loader.get_template('transaction_new.html')
    return HttpResponse(template.render(context, request))


##################################
# functions tool for page views
##################################

def transaction_variable_month(request):
    year_month = datetime.strptime(request.GET.get('month', datetime.now().strftime('%Y%m')), '%Y%m')
    context = {'month': datetime.strftime(year_month, '%B %Y')}
    template = loader.get_template('transaction_variable_month.html')
    return HttpResponse(template.render(context, request))


def subtract_one_month(datetime_0):
    """
    Move a datetime to previous month
    Args:
        datetime_0 (datetime):

    Returns:
        datetime
    """
    day = datetime_0.day
    month = datetime_0.month
    datetime_1 = datetime_0.replace(day=1)
    datetime_2 = datetime_1 - timedelta(days=1)

    try:
        datetime_3 = datetime_2.replace(day=day)
    except ValueError:
        if month - 1 == 2:
            day = 28
        if month - 1 in [4, 6, 9, 11]:
            day = 30
        datetime_3 = datetime_2.replace(day=day)

    return datetime_3


##################################
# Home page views
##################################

class HomePageView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        month = self.request.GET.get('month')
        if month is None:
            current_date = datetime.now()
        else:
            current_date = datetime.strptime(month, '%Y%m')

        var_month_transaction = Transaction.objects.filter(
            mode='VAR',
            state='RAL',
            real_date__year=current_date.strftime('%Y'),
            real_date__month=current_date.strftime('%m')
        ).order_by('-real_date')
        context['var_month_transaction'] = var_month_transaction

        total_amount = var_month_transaction.aggregate(Sum('quantity'))['quantity__sum']
        total_amount = 0 if total_amount is None else total_amount
        context['total_amount'] = {'value': total_amount, 'percent': (total_amount * 100) / 500}

        fdx_month_transaction = Transaction.objects.filter(
            mode='FXD',
            state='RAL',
            real_date__year=current_date.strftime('%Y'),
            real_date__month=current_date.strftime('%m')
        ).order_by('-real_date')
        context['fdx_month_transaction'] = fdx_month_transaction

        fxd_total_amount = fdx_month_transaction.aggregate(Sum('quantity'))['quantity__sum']
        context['fxd_total_amount'] = fxd_total_amount

        sum_amount_categories = Transaction.objects.values('category__name').annotate(Sum('quantity')).filter(
            mode='VAR',
            state='RAL',
            real_date__year=current_date.strftime('%Y'),
            real_date__month=current_date.strftime('%m')
        ).order_by('-quantity__sum')
        context['sum_amount_categories'] = sum_amount_categories

        past_date = subtract_one_month(current_date)
        context['current_month'] = {'label': current_date.strftime('%B %Y'), 'link': current_date.strftime('%Y%m')}

        context['past_month'] = {'label': past_date.strftime('%B %Y'), 'link': past_date.strftime('%Y%m')}

        plans = Plan.objects.filter(active='ENBL')
        context['plans'] = plans

        return context


##################################
# Category views
##################################

class CategoryList(ListView):
    model = Category


class CategoryCreate(CreateView):
    model = Category
    fields = ['name']
    success_url = '/category'


class CategoryUpdate(UpdateView):
    model = Category
    fields = ['name']
    template_name_suffix = '_update_form'
    success_url = '/category'


################################
# PLan views
################################

class PlanList(ListView):
    model = Plan


class PlanCreate(CreateView):
    model = Plan
    fields = ['quantity', 'category', 'description', 'month_day', 'active']
    success_url = '/plan'
