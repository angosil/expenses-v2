from django import forms
from .models import Transaction


class TransactionForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = ('quantity', 'category', 'sub_category', 'mode', 'state', 'description')
