from django.db import models


class Transaction(models.Model):
    FIXED = 'FXD'
    VARIABLE = 'VAR'
    MODE_CHOICE = ((FIXED, 'Fixed'), (VARIABLE, 'Variable'))

    ESTIMATED = 'EST'
    REAL = 'RAL'
    STATE_CHOICE = ((ESTIMATED, 'Estimated'), (REAL, 'Real'))

    quantity = models.DecimalField(decimal_places=2, max_digits=8)
    category = models.ForeignKey('Category', on_delete=models.CASCADE)
    sub_category = models.ForeignKey('SubCategory', on_delete=models.CASCADE, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    mode = models.CharField(
        max_length=3,
        choices=MODE_CHOICE,
        default=VARIABLE
    )
    real_date = models.DateField(default=None, null=True, blank=True)
    state = models.CharField(
        max_length=3,
        choices=STATE_CHOICE,
        default=REAL
    )
    computable = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def real(self):
        return self.real_date

    @property
    def no_computable(self):
        return 'NO COMPUTABLE' if not self.computable else ''

    @property
    def created(self):
        return self.created_at

    @property
    def updated(self):
        return self.updated_at


class Category(models.Model):
    name = models.CharField(max_length=20, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class TransactionCategorySummary(Transaction):
    class Meta:
        proxy = True
        verbose_name = 'Transaction Category Summary'
        verbose_name_plural = 'Transactions Category Summary'


class SubCategory(models.Model):
    name = models.CharField(max_length=20, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Plan(models.Model):
    ENABLED = 'ENBL'
    DISABLED = 'DSBL'
    ACTIVE_CHOICE = ((ENABLED, 'Enabled'), (DISABLED, 'Disabled'))

    quantity = models.DecimalField(decimal_places=2, max_digits=8)
    category = models.ForeignKey('Category', on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)
    date = models.DateField()
    active = models.CharField(
        max_length=4,
        choices=ACTIVE_CHOICE,
        default=ENABLED)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def created(self):
        return self.created_at

    def updated(self):
        return self.updated_at
