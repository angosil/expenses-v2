from django.contrib import admin
from django.db.models import DateField, Sum, Count, Min, Max
from django.db.models.functions import Trunc
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter
from .models import Transaction, Category, SubCategory, Plan, TransactionCategorySummary


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    search_fields = ('description',)
    ordering = ('-real_date',)
    date_hierarchy = 'real_date'
    list_display = (
        'quantity', 'category', 'sub_category', 'mode', 'real_date', 'state', 'no_computable', 'created_at',
        'updated_at')
    list_filter = (
        'mode', 'state', ('category', RelatedDropdownFilter), ('sub_category', RelatedDropdownFilter), 'real_date',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at', 'updated_at')


@admin.register(SubCategory)
class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at', 'updated_at')


@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    pass


@admin.register(TransactionCategorySummary)
class TransactionCategorySummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/transaction_summary_change_list.html'
    date_hierarchy = 'real_date'
    list_filter = (
        'mode', 'state', ('category', RelatedDropdownFilter), ('sub_category', RelatedDropdownFilter), 'real_date',)

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context
        )

        try:
            qs = response.context_data['cl'].queryset.filter(computable=True)
        except (AttributeError, KeyError):
            return response

        metrics = {
            'total': Count('id'),
            'quantity__sum': Sum('quantity'),
        }

        response.context_data['summary'] = list(
            qs.values('category__name').annotate(**metrics).order_by('-quantity__sum')
        )

        response.context_data['summary_total'] = dict(
            qs.aggregate(**metrics)
        )

        period = get_next_in_date_hierarchy(
            request,
            self.date_hierarchy,
        )
        response.context_data['period'] = period

        summary_over_time = qs.annotate(
            period=Trunc(
                self.date_hierarchy,
                period,
                output_field=DateField(),
            ),
        ).values('period').annotate(total=Sum('quantity')).order_by('period')

        summary_range = summary_over_time.aggregate(
            low=Min('total'),
            high=Max('total'),
        )
        high = summary_range.get('high', 0)
        low = summary_range.get('low', 0)

        response.context_data['summary_over_time'] = [
            {'period': x['period'],
             'total': x['total'] or 0,
             'pct': ((x['total'] or 0) - low) / (high - low) * 100 if high > low else 0} for x in summary_over_time
        ]

        return response


def get_next_in_date_hierarchy(request, date_hierarchy):
    if date_hierarchy + '__day' in request.GET:
        return 'hour'
    if date_hierarchy + '__month' in request.GET:
        return 'day'
    if date_hierarchy + '__year' in request.GET:
        return 'week'
    return 'month'
