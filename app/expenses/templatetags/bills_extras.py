from django import template

register = template.Library()


@register.filter
def percent_of(part, whole):
    try:
        return"{:.2f}".format(float(part) / float(whole) * 100)
    except (ValueError, ZeroDivisionError):
        return ""
