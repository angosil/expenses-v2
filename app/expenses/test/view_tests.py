import pytest
from datetime import datetime
from app.expenses.views import index, transaction_variable_month, subtract_one_month
from django.core.management import call_command


@pytest.mark.django_db
def test_index(rf):
    request = rf.get('')
    response = index(request)
    assert response.status_code == 200


@pytest.fixture(scope='session')
def django_db_setup(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        call_command('loaddata', 'category.json', 'sub_category.json', 'out_payment.json')


def test_month_variable_transaction(rf, db, django_db_setup):
    request = rf.get('?month=201804')
    response = transaction_variable_month(request)
    assert response.status_code == 200
    assert b'April 2019' in response.content


def test_subtract_one_month():
    datetime_1 = datetime.strptime('2018 01 23', '%Y %m %d')
    datetime_2 = datetime.strptime('2017 12 23', '%Y %m %d')
    assert datetime_2 == subtract_one_month(datetime_1)


def test_subtract_one_month_february():
    datetime_1 = datetime.strptime('2018 03 31', '%Y %m %d')
    datetime_2 = datetime.strptime('2018 02 28', '%Y %m %d')
    assert datetime_2 == subtract_one_month(datetime_1)
