import pytest
from decimal import Decimal
from app.expenses.models import Transaction, Category, SubCategory


@pytest.mark.django_db
class TestTransaction:
    def test_save(self):
        transaction = Transaction.objects.create(
            quantity=Decimal(50.44),
            category=Category.objects.create(name='test'),
            sub_category=SubCategory.objects.create(name='test')
        )
        assert transaction.quantity == Decimal(50.44)
        assert transaction.category == Category.objects.get(pk=1)
